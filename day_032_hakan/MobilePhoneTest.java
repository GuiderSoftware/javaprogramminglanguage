package day_032_hakan;

public class MobilePhoneTest {
    public static void main(String[] args) {
    MobilePhone phone = new MobilePhone("Iphone","13", "black",
            true, 1000, 256000, 5.6);

        phone.call();
        phone.saveToMemory(1000);
        phone.takePhoto();
        phone.takePhoto();
        phone.takePhoto();
        phone.takePhoto(50);
        System.out.println(phone.getBatteryStatus());
        System.out.println(phone.getFreeMemory());
        phone.chargePhone(30);
        phone.increaseMemory(32000);
        System.out.println(phone.getBatteryStatus());
        System.out.println(phone.getFreeMemory());

    }
}
