package day_040_hakan.QC;

import java.util.Arrays;

public class Answers {
    private boolean[] answers;

    public Answers(){
        answers = new boolean[10];
    }

    public Answers(boolean a1,boolean a2,boolean a3,boolean a4,boolean a5,boolean a6,boolean a7,boolean a8,boolean a9,boolean a10){
        answers = new boolean[]{a1, a2, a3, a4, a5, a6, a7, a8, a9, a10};
    }

    public int getTrueAnswerCount(){
        int counter=0;
        for (boolean answer: answers) {
            if(answer) counter++;
        }
        return counter;
    }

    public int getFalseAnswerCount(){
        int counter=0;
        for (boolean answer: answers) {
            if(!answer) counter++;
        }
        return counter;
    }

    public String getAnswers(){
        return Arrays.toString(answers);
    }

    public void setAnswerInOrder(int order, boolean answer){
        answers[order-1] = answer;
    }
}
