package day_033_hakan;

public class StudentTest {

    public static void main(String[] args) {
        System.out.println("***************** static variable test ****************");
        System.out.println(Student.schoolName); // Grundschule
        Student student_1 = new Student("Ahmet", 123);
        System.out.println(Student.schoolName); // Karl Marx Grundschule
        Student.schoolName = "Dunya Okulu";
        System.out.println(student_1.getSchoolName()); // Dunya Okulu
        System.out.println(Student.schoolName); // Dunya Okulu
        Student student_2 = new Student("Ahmet", 123);
        Student student_3 = new Student("Ahmet");
        Student student_4 = new Student( 123);
        Student student_5 = new Student( );
        System.out.println(student_4.getCountsOfStudents());

    }
}
