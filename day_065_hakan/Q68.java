package day_065_hakan;

import java.io.IOException;

public class Q68 {
    public static void main(String[] args){
        try{
            method1();
        } catch (MyException me){
            System.out.println("A");
        }
    }


    public static void method1(){
        try{
            throw 3>10 ? new MyException() : new IOException();
           // throw 3<10 ? new MyException() : new IOException(); print "B"
        } catch (IOException ie){
            System.out.println("I"); // print "I"
          //  throw new MyException(); print "A"
          //  throw new IOException();
        } catch(Exception e) {
            System.out.println("B");
        }
    }
}

class MyException extends RuntimeException { }

